package com.dwbook.phonebook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.dwbook.phonebook.resources.ContactResource;
import org.skife.jdbi.v2.DBI;
import io.dropwizard.jdbi.DBIFactory;


public class App extends Application<PhonebookConfiguration>
{
    //extended class has been changed from Application<Configutration> to
    //App extends Application<PhonebookConfiguration> so tha we caN USE our new class phonebookconfiguration as our configuration proxy

    //logger keeps a record of what happened in the program
    //so, if there is a crash or a bug latter on we can check what haooened.
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    //Implement the abstract methods of the Service class
    @Override
    public void initialize(Bootstrap<PhonebookConfiguration> b) {}
    //The initialize method is tasked with bootstrapping, possibly loading additional components and generally
    //preparing the runtime environment of the application

    @Override
    public void run(PhonebookConfiguration c,Environment e) throws Exception {
       LOGGER.info("Method App#run() called");
       for(int i=0;i<c.getMessageRepetitions();i++)
        System.out.println(c.getMessage());

        //System.out.println(c.getAdditionalMessage());
        // Create a DBI factory and build a JDBI instance
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory
                .build(e, c.getDataSourceFactory(), "postgresql");

       //Add the rsource to the environment
        e.jersey().register(new ContactResource(jdbi));
    }

    public static void main( String[] args ) throws Exception {new App().run(args);}
    {
        System.out.println( "Hello World!" );
    }
}
