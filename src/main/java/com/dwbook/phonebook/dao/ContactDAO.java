package com.dwbook.phonebook.dao;

import org.skife.jdbi.v2.sqlobject.*;
import com.dwbook.phonebook.representations.Contact;
import com.dwbook.phonebook.dao.mappers.ContactMapper;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;


public interface ContactDAO {
    //using SQLQuery annotation as we are fetching info from the database
    @SqlQuery("select * from contact where id = :id")

    //Mapper classes facilitate the mapping of a resultset database row to an object
     @Mapper(ContactMapper.class)
     Contact getContactById(@Bind("id") int id);

    @GetGeneratedKeys
    //@GetGeneratedKeys annotation is used in order to retrieve the value of the primary key of the newly
    //inserted row; in this case, the value of the id field
    @SqlUpdate("insert into contact (firstName,lastName,phone) values (:firstName, :lastName, :phone)")
    int createContact(@Bind("firstName") String firstName,
                      @Bind("lastName") String lastName,
                      @Bind("phone") String phone);

    @SqlUpdate("update contact set firstName = :firstName, lastName = :lastName, phone = :phone where id = :id")
    void updateContact(@Bind("id") int id,
                       @Bind("firstName") String firstName,
                       @Bind("lastName") String lastName,
                       @Bind("phone") String phone);

    @SqlUpdate("delete from contact where id = :id")
    void deleteContact(@Bind("id") int id);
}
