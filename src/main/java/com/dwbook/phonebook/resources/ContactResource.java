package com.dwbook.phonebook.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import com.dwbook.phonebook.representations.Contact;
import com.dwbook.phonebook.dao.ContactDAO;
import org.skife.jdbi.v2.DBI;
import java.net.URI;
import java.net.URISyntaxException;

@Path("/contact")
@Produces(MediaType.APPLICATION_JSON)
public class ContactResource {
    //method for obtaining information regarding the stored contact

    @GET
    @Path("/{id}")
    public Response getContact(@PathParam("id") int id) {
        Contact contact = contactDao.getContactById(id);
        return Response
                .ok(contact)
                .build();
    }
    //methods for creating, deleting and updating contacts.
    //Since nothing is appended to our base URI, this method does not need to be annotated with @Path .
    @POST
    public Response createContact(Contact contact) throws URISyntaxException
    {
        //nsert the new contact in the database, retrieve its id , and use it to construct its URI, passing it as a
        //parameter to the Response#created() method.
        int newContactId = contactDao.createContact(contact.getFirstName(), contact.getLastName(), contact.getPhone());
        return Response
                .created(new URI(String.valueOf(newContactId)))
                .build();
    }

    @PUT
    @Path("/{id}")
    public Response updateContact(
            @PathParam("id") int id,
            Contact contact){
        //Update the contact with the provided ID

        contactDao.updateContact(id, contact.getFirstName(), contact.getLastName(), contact.getPhone());
        return Response
                .ok(new Contact(id,contact.getFirstName(),contact.getLastName(),contact.getPhone()))
                .build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteContact(@PathParam("id") int id){
        contactDao.deleteContact(id);
        return Response.noContent().build();
    }


    private final ContactDAO contactDao;
    public ContactResource(DBI jdbi) {
        contactDao = jdbi.onDemand(ContactDAO.class);
    }


}

